#  __  __        ____ _           _    ____       _   
# |  \/  |_   _ / ___| |__   __ _| |_ / ___|_ __ | |_ 
# | |\/| | | | | |   | '_ \ / _` | __| |  _| '_ \| __|
# | |  | | |_| | |___| | | | (_| | |_| |_| | |_) | |_ 
# |_|  |_|\__, |\____|_| |_|\__,_|\__|\____| .__/ \__|
#         |___/                            |_|        
# 
# by Stephan Raabe (2023)
# -----------------------------------------------------
# myChatGpt script
# modify the path to the openai.yaml
# -----------------------------------------------------

import os
import openai
import pyperclip
import yaml
import readline
import click

from pathlib import Path
from yaml import load,CLoader as Loader

# Retrieve API key from environment variable
api_key = os.environ.get("OPENAI_API_KEY")

if api_key is None:
    raise ValueError("OPENAI_API_KEY environment variable is not set.")

# Set the API key
openai.api_key = api_key

# Function to send a request to OpenAI API
def sendRequest():
    # Use the global openai object or create a new client instance
    # depending on your script structure
    completion = openai.Completion.create(
        engine="davinci-codex",
        prompt="Translate the following English text to French: '{}'",
        max_tokens=150
    )
# Process the response as needed
    print(completion.choices[0].text.strip())
        #return response.choices[0].text.strip()

# Example usage
# user_input = "Hello, ChatGPT!"
# response = chat_with_gpt(user_input)

# print("User:", user_input)
# print("ChatGPT:", response)


# Path to your openai.yaml in your home directory
openaipath = "/openai.yaml"
# openaipath = "/mychatgpt/openai.yaml"
chat_logfile = "~/mychatgpt/private/chatlog.txt"

# Get home path
home = str(Path.home())

# Load openai api key
# with open(home + openaipath, "r") as ymlfile:
#   cfg = yaml.load(ymlfile, Loader=Loader)

# Set up the model and prompt
# model_engine = "text-davinci-003"

@click.command()
def main():
    sendRequest()

    prompt = str(input("Hello, how can I help you? "))
    print('...')

    # Generate a response
    completion = openai.completions.create(engine=engine,
    prompt=prompt,
    max_tokens=1024,
    n=1,
    stop=None,
    temperature=0.5)

    #response = completion.choices[0].text
    print(response)

    pyperclip.copy(response)
    print("...")
    print("Output has been copied to the clipboard!")
    
    c = input("Do you want to store the question and answer in the Chat Logfile (y/n)? (default: y) ")
    if (c != "n"):
        f = open (home + chat_logfile, "a")
        f.write("Question: " + prompt)
        f.write(response + "\n")
        f.write(" \n")
        f.close()
        print("Output written to chat logfile " + chat_logfile + "!")

if __name__ == '__main__':
    sendRequest()

