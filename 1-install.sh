#!/bin/sh

# ------------------------------------------------------
# Install Script for MyChatGpt
# ------------------------------------------------------
yay -S python-pipx --noconfirm -y

sudo pacman -S python-openai -y --noconfirm
sudo pacman -S python-pyperclip -y --noconfirm
sudo pacman -S python-click -y --noconfirm
sudo pacman -S python-rich -y --noconfirm
